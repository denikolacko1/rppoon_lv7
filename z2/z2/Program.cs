

namespace z2
{
    class Program
    {
        static void Main(string[] args)
        {
            LinearSearch linearSearch = new LinearSearch();
            double[] array = new double[] { 0, -3.14, 6, -9, 99, 44.44, -188, -76, 999, 10, 14 };
            NumberSequence numbers = new NumberSequence(array);
            numbers.SetSearchStrategy(linearSearch);
            System.Console.WriteLine("Number -76 is founded at index = " + numbers.Search(-76));
        }
    }
}
