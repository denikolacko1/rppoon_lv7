﻿
namespace z3
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleLogger console = new ConsoleLogger();
            SystemDataProvider data = new SystemDataProvider();
            data.Attach(console);
            while (!false)
            {
                data.GetCPULoad();
                data.GetAvailableRAM();
                System.Threading.Thread.Sleep(1000);
            }


        }
    }
}
