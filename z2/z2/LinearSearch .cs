﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z2
{
    class LinearSearch : SearchStrategy
    {
        public override int Search(double[] array, double item)
        {
            for (int i = 0; i < array.Length; i++)
                if (array[i] == item)
                    return i;
            return -1;
        }
    }
}
