﻿

namespace Z1
{
    class Program
    {
        static void Main(string[] args)
        {
            BubbleSort bubbleSorting = new BubbleSort();
            CombSort combSorting = new CombSort();
            SequentialSort sequentialSorting = new SequentialSort();
            double[] sequenceOfNumbers = new double[] { 2, 6, 1, 3.14, 2.63, -1, 0,302.31,-12.32,94.21 };
            NumberSequence BubbleSequence = new NumberSequence(sequenceOfNumbers);
            NumberSequence CombSequence = new NumberSequence(sequenceOfNumbers);
            NumberSequence SequentialSequence = new NumberSequence(sequenceOfNumbers);

            System.Console.WriteLine("Array of numbers before Bubble sort = \n"+ BubbleSequence.ToString());
            BubbleSequence.SetSortStrategy(bubbleSorting);
            BubbleSequence.Sort();
            System.Console.WriteLine("Results after sorting with bubble sort \n"+ BubbleSequence.ToString());

            System.Console.WriteLine("Array of numbers before comb sorting = \n"+ CombSequence.ToString());
            CombSequence.SetSortStrategy(combSorting);
            CombSequence.Sort();
            System.Console.WriteLine("Results after sorting with comb sorting \n" + CombSequence.ToString());

            System.Console.WriteLine("Array of numbers before Sequntial sorting =\n"+ SequentialSequence.ToString());
            SequentialSequence.SetSortStrategy(sequentialSorting);
            SequentialSequence.Sort();
            System.Console.WriteLine("Results after sorting with sequential sorting \n"+ SequentialSequence.ToString());



        }
    }
}
